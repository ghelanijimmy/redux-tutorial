import React, {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useReducer,
  useRef,
} from "react";
import { reducer } from "./reducer";
import { middleware } from "./setupMiddleware";
import { countMiddleware } from "./middleware";

const initialState = {
  amount: 1,
  username: "Jimmy",
  age: 30,
  lastValue: [],
  test: "test",
};

export const context = createContext(initialState);
const { Provider } = context;

const useEnhancedReducer = (
  reducer,
  initialState,
  initializer,
  middlewares = []
) => {
  const lastState = useRef(initialState);
  const getState = useCallback(() => lastState.current, []);

  const [, dispatcher] = [
    ...useReducer(
      (state, action) => (lastState.current = reducer(state, action)),
      initialState,
      initializer
    ),
  ];

  const dispatch = useMemo(() => {
    return middleware(dispatcher, getState, middlewares);
  }, [dispatcher, getState, middlewares]);

  useEffect(() => {
    console.log("Created context dispatch");
  }, [dispatch]);

  useEffect(() => {
    console.log("Created context getState");
  }, [getState]);

  return [getState, dispatch];
};

const ContextProvider = ({ children }) => {
  const middlewares = useMemo(() => [countMiddleware], []);
  const [getState, dispatch] = useEnhancedReducer(
    reducer,
    initialState,
    () => initialState,
    middlewares
  );

  useEffect(() => {
    console.log("provider state and dispatch for context are updating");
  }, [getState, dispatch]);

  return <Provider value={[getState, dispatch]}>{children}</Provider>;
};

export const useCustomContext = () => {
  const [getState, dispatch] = useContext(context);
  if (!getState) {
    throw new Error("Must be called from withing a Provider-wrapped component");
  }
  return { getState, dispatch };
};

export default ContextProvider;
