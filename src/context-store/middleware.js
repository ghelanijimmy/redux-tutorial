import ContextTypes from "./types";

export const countMiddleware = ({ dispatch, getState }) => (action) => {
  if (
    action?.type === ContextTypes.ADD ||
    action?.type === ContextTypes.SUBTRACT
  ) {
    dispatch({ type: ContextTypes.LAST_VALUE });
  }
};
