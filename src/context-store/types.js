const ADD = "add";
const SUBTRACT = "subtract";
const LAST_VALUE = "last_value";
const CHANGE_USER = "change_user";
const CHANGE_AGE = "change_age";

const ContextTypes = {
  ADD,
  SUBTRACT,
  LAST_VALUE,
  CHANGE_AGE,
  CHANGE_USER,
};
export default ContextTypes;
