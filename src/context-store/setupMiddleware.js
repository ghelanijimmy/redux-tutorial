const handleDispatch = (dispatch, getState) => (action) => {
  if (typeof action === "function" && action() instanceof Promise) {
    return action(getState).then(dispatch);
  } else if (typeof action === "function") {
    return action(dispatch, getState);
  } else return dispatch(action);
};
const handleMiddleware = ({ dispatch, getState, action }, middlewares) => {
  middlewares.forEach((middleware) => {
    middleware({ dispatch, getState })(action);
  });
};
export const middleware = (dispatch, getState, middlewares = []) => (
  action
) => {
  const dispatchFunction = handleDispatch(dispatch, getState);

  handleMiddleware(
    { dispatch: dispatchFunction, getState, action },
    middlewares
  );

  dispatchFunction(action);
};
