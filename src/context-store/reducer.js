import ContextTypes from "./types";

/**
 * Reducer function
 * @param state
 * @param action
 * @type {any}
 */
export const reducer = (state, action) => {
  const { type, payload } = action;
  switch (type) {
    case ContextTypes.ADD:
      return {
        ...state,
        amount: state.amount + 1,
      };
    case ContextTypes.SUBTRACT:
      return {
        ...state,
        amount: state.amount - 1,
      };
    case ContextTypes.LAST_VALUE:
      return {
        ...state,
        lastValue: [...state.lastValue, state.amount],
      };
    case ContextTypes.CHANGE_USER:
      return {
        ...state,
        username: payload,
      };
    case ContextTypes.CHANGE_AGE:
      return {
        ...state,
        age: payload,
      };

    default:
      return { ...state };
  }
};
