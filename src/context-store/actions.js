import ContextTypes from "./types";

export const addContext = () => ({
  type: ContextTypes.ADD,
});
export const subtractContext = () => ({
  type: ContextTypes.SUBTRACT,
});
export const changeUser = () => (
  dispatch = () => null,
  getState = () => null
) => {
  const state = getState();
  const username = state?.username === "Jimmy" ? "Stan" : "Jimmy";
  setTimeout(() => {
    dispatch({
      type: ContextTypes.CHANGE_USER,
      payload: username,
    });
  }, 1000);
};
export const changeAge = () => (getState = () => null) => {
  return new Promise((resolve) => {
    const state = getState();
    const age = state?.age;
    setTimeout(() => {
      resolve({
        type: ContextTypes.CHANGE_AGE,
        payload: age === 30 ? 31 : 30,
      });
    }, 1000);
  });
};
