import { types } from "./types";

const initialState = {
  lastValues: [],
  username: "Jimmy",
  age: 30,
  test: "test",
  amount: 1,
};

export const appReducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case types.ADD:
      return {
        ...state,
        amount: state.amount + 1,
      };
    case types.SUBTRACT:
      return {
        ...state,
        amount: state.amount - 1,
      };
    case types.SET_NAME:
      return {
        ...state,
        username: payload,
      };
    case `${types.SET_AGE}`:
      return {
        ...state,
        age: payload,
      };
    case types.LAST_VALUE:
      return {
        ...state,
        lastValues: [...state.lastValues, state.amount],
      };
    default:
      return { ...state };
  }
};
