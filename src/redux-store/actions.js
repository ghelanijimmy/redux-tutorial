import { types } from "./types";

export const subtract = (amount) => ({
  type: types.SUBTRACT,
  payload: amount,
});

export const add = (amount) => ({
  type: types.ADD,
  payload: amount,
});

export const setName = () => {
  return (dispatch, getState) => {
    const state = getState();
    const username = state?.username === "Jimmy" ? "Stan" : "Jimmy";
    setTimeout(() => {
      dispatch({
        type: types.SET_NAME,
        payload: username,
      });
    }, 1000);
  };
};

export const setAge = () => (dispatch = () => null, getState = () => null) => {
  return new Promise((resolve) => {
    const state = getState();
    const age = state?.age;
    setTimeout(() => {
      resolve(
        dispatch({
          type: types.SET_AGE,
          payload: age === 30 ? 31 : 30,
        })
      );
    }, 1000);
  });
};
