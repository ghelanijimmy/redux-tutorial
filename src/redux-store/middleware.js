import { types } from "./types";

export const customMiddleware = ({ dispatch }) => (next) => (action) => {
  if (action.type === types.ADD || action.type === types.SUBTRACT) {
    dispatch({
      type: types.LAST_VALUE,
    });
  }
  next(action);
};
