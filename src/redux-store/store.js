import React, { useEffect } from "react";
import { appReducer } from "./reducer";
import { customMiddleware } from "./middleware";
import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import thunk from "redux-thunk";
import promise from "redux-promise-middleware";
import { Provider } from "react-redux";

const middleware = [
  ...getDefaultMiddleware(),
  thunk,
  promise,
  customMiddleware,
];

export const store = configureStore({
  reducer: appReducer,
  middleware,
});

export const ReduxProvider = ({ children }) => {
  const { getState, dispatch } = store;

  useEffect(() => {
    console.log("Created redux dispatch");
    console.log("Created redux getState");
    console.log("Redux dispatch and state are updating");
  }, [getState, dispatch]);
  return <Provider store={store}>{children}</Provider>;
};
