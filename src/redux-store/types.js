export const types = {
  ADD: "ADD",
  SUBTRACT: "SUBTRACT",
  SET_NAME: "SET_NAME",
  SET_AGE: "SET_AGE",
  LAST_VALUE: "LAST_VALUE",
};
