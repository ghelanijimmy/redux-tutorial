import React from "react";
import Content from "./components/redux-components/Content";
import ContextContent from "./components/context-components/ContextContent";
import ContextProvider from "./context-store/store";
import { SecondContent } from "./components/redux-components/RenderRefUpdates";
import { SecondContextContent } from "./components/context-components/ContextRenderRefUpdates";
import { ReduxProvider } from "./redux-store/store";

const App = () => {
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-around",
        fontSize: "14px",
      }}
    >
      <ReduxProvider>
        <div>
          <Content />

          <div>
            <h3>Direct child of Provider:</h3> <SecondContent />
          </div>
        </div>
      </ReduxProvider>
      <ContextProvider>
        <div>
          <ContextContent />
          <div>
            <h3>Direct child of Provider:</h3> <SecondContextContent />
          </div>
        </div>
      </ContextProvider>
    </div>
  );
};
App.propTypes = {};

export default App;
