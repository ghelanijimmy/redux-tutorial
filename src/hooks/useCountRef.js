import React, { useEffect } from "react";
import { useRef } from "react";

const useCountRef = () => {
  const ref = useRef();
  useEffect(() => {
    ref.current.textContent = Number(ref.current.textContent || 0) + 1;
  });
  return <span ref={ref} />;
};
export default useCountRef;
