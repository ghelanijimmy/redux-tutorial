import React from "react";
import Test from "./Test";
import TestTwo from "./TestTwo";
import MemoTest from "./MemoTest";
import { useSelector } from "react-redux";
import useCountRef from "../../hooks/useCountRef";

export const SecondContent = () => {
  const num = useCountRef();
  return <p>Ref Update: {num}</p>;
};

const RenderRefUpdates = () => {
  const { test } = useSelector((state) => state);
  return (
    <div>
      <div>
        <h3>Inside Provider's Child Component</h3>
        <div>
          <p>
            <u>Pass props to child component:</u>
          </p>
          <Test test={test} />
          <hr />
          <p>
            <u>Child component calling 'useSelector'</u>
          </p>
          <TestTwo />
          <hr />
          <p>
            <u>Pass props to memoized child:</u>
          </p>
          <MemoTest test={test} />
          <hr />
        </div>
      </div>
    </div>
  );
};
RenderRefUpdates.propTypes = {};

export default RenderRefUpdates;
