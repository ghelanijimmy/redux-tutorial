import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { add, subtract } from "../../redux-store/actions";
import useCountRef from "../../hooks/useCountRef";

const Counter = () => {
  const amount = useSelector((state) => state?.amount);

  const lastValue = useSelector((state) => state?.lastValues);
  /**
   * Dispatch function
   * @type {any}
   */
  const dispatch = useDispatch();
  const num = useCountRef();
  return (
    <div>
      <button onClick={() => dispatch(add())}>Add</button>
      <button onClick={() => dispatch(subtract())}>Subtract</button>
      <p>{amount}</p>
      {lastValue?.length ? (
        <div>
          <h4>Update with middleware:</h4>
          <p>History:</p>
          <p>
            {lastValue?.map((last, i) => (
              <span key={i}>
                {last}
                {i === lastValue.length - 1 ? "" : ","}
              </span>
            ))}
          </p>
        </div>
      ) : null}

      <p>Ref Update: {num}</p>
    </div>
  );
};

export default Counter;
