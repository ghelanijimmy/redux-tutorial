import React, { memo } from "react";
import PropTypes from "prop-types";
import useCountRef from "../../hooks/useCountRef";

const MemoTest = memo(({ test }) => {
  const num = useCountRef();

  return (
    <div>
      <p>Props: {test}</p>
      <p>Ref Update: {num}</p>
    </div>
  );
});
MemoTest.propTypes = {
  test: PropTypes.string,
};

export default MemoTest;
