import React from "react";
import PropTypes from "prop-types";
import useCountRef from "../../hooks/useCountRef";

const Test = ({ test }) => {
  const num = useCountRef();
  return (
    <div>
      <p>Passed Props: {test}</p>
      <p>Ref Update: {num}</p>
    </div>
  );
};
Test.propTypes = {
  test: PropTypes.string,
};

export default Test;
