import React from "react";
import { useDispatch } from "react-redux";
import { setAge, setName } from "../../redux-store/actions";
import useCountRef from "../../hooks/useCountRef";

const ChangeNameAge = () => {
  const num = useCountRef();

  /**
   * Dispatch function
   * @type {any}
   */
  const dispatch = useDispatch();
  return (
    <div>
      <button onClick={() => dispatch(setName())}>Change name (timeout)</button>
      <button onClick={() => dispatch(setAge())}>Change age (Promise)</button>
      <p>Ref Update: {num}</p>
    </div>
  );
};
ChangeNameAge.propTypes = {};

export default ChangeNameAge;
