import React from "react";
import useCountRef from "../../hooks/useCountRef";
import { useSelector } from "react-redux";

const TestTwo = () => {
  const test = useSelector((state) => state.test);
  const num = useCountRef();

  return (
    <div>
      <p>Selector: {test}</p>
      <p>Ref Update: {num}</p>
    </div>
  );
};
TestTwo.propTypes = {};

export default TestTwo;
