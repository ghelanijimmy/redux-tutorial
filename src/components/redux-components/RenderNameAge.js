import React, { Fragment } from "react";
import { useSelector } from "react-redux";
import useCountRef from "../../hooks/useCountRef";

const RenderNameAge = () => {
  const num = useCountRef();
  const username = useSelector((state) => state?.username);
  const age = useSelector((state) => state?.age);
  return (
    <Fragment>
      <h2>
        {username} {age}
      </h2>
      <p>Ref Update: {num}</p>
    </Fragment>
  );
};

export default RenderNameAge;
