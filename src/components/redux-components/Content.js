import React from "react";

import RenderNameAge from "./RenderNameAge";
import Counter from "./Counter";
import ChangeNameAge from "./ChangeNameAge";
import RenderRefUpdates from "./RenderRefUpdates";

const Content = () => {
  return (
    <div>
      <h1>Redux</h1>
      <RenderNameAge />
      <Counter />
      <ChangeNameAge />
      <hr />
      <RenderRefUpdates />
    </div>
  );
};

export default Content;
