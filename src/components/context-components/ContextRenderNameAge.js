import React, { Fragment } from "react";
import useCountRef from "../../hooks/useCountRef";
import { useCustomContext } from "../../context-store/store";

const ContextRenderNameAge = () => {
  const num = useCountRef();
  const { getState } = useCustomContext();
  const { username, age } = getState();
  return (
    <Fragment>
      <h2>
        {username} {age}
      </h2>
      <p>Ref Update: {num}</p>
    </Fragment>
  );
};

export default ContextRenderNameAge;
