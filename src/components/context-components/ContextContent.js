import React from "react";
import ContextRenderNameAge from "./ContextRenderNameAge";
import ContextCounter from "./ContextCounter";
import ContextChangeNameAge from "./ContextChangeNameAge";
import ContextRenderRefUpdates from "./ContextRenderRefUpdates";

const ContextContent = () => {
  return (
    <div>
      <h1>Context</h1>
      <ContextRenderNameAge />
      <ContextCounter />
      <ContextChangeNameAge />
      <hr />
      <ContextRenderRefUpdates />
    </div>
  );
};
export default ContextContent;
