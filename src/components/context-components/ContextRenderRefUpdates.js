import React from "react";
import ContextTest from "./ContextTest";
import ContextTestTwo from "./ContextTestTwo";
import ContextMemoTest from "./ContextMemoTest";
import useCountRef from "../../hooks/useCountRef";
import { useCustomContext } from "../../context-store/store";

export const SecondContextContent = () => {
  const num = useCountRef();
  return <p>Ref Update: {num}</p>;
};

const ContextRenderRefUpdates = () => {
  const { getState } = useCustomContext();
  const { test } = getState();
  return (
    <div>
      <div>
        <h3>Inside Provider's Child Component</h3>
        <div>
          <p>
            <u>Pass props to child component</u>
          </p>
          <ContextTest test={test} />
          <hr />
          <p>
            <u>Child component calling 'useContext'</u>
          </p>
          <ContextTestTwo />
          <hr />
          <p>
            <u>Pass props to memoized child:</u>
          </p>
          <ContextMemoTest test={test} />
          <hr />
        </div>
      </div>
    </div>
  );
};

export default ContextRenderRefUpdates;
