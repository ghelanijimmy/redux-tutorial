import React from "react";
import { useCustomContext } from "../../context-store/store";
import { addContext, subtractContext } from "../../context-store/actions";
import useCountRef from "../../hooks/useCountRef";

const ContextCounter = () => {
  const { getState, dispatch } = useCustomContext();
  const { amount, lastValue } = getState();
  const num = useCountRef();
  return (
    <div>
      <button onClick={() => dispatch(addContext())}>Add</button>
      <button onClick={() => dispatch(subtractContext())}>Subtract</button>
      <p>{amount}</p>
      {lastValue?.length ? (
        <div>
          <h4>Update with middleware:</h4>
          <p>History:</p>
          <p>
            {lastValue?.map((last, i) => (
              <span key={i}>
                {last}
                {i === lastValue.length - 1 ? "" : ","}
              </span>
            ))}
          </p>
        </div>
      ) : null}
      <p>Ref Update: {num}</p>
    </div>
  );
};

export default ContextCounter;
