import React, { memo } from "react";
import PropTypes from "prop-types";
import useCountRef from "../../hooks/useCountRef";

const ContextMemoTest = memo(({ test }) => {
  const num = useCountRef();

  return (
    <div>
      <p>Passed Props: {test}</p>
      <p>Ref Update: {num}</p>
    </div>
  );
});
ContextMemoTest.propTypes = {
  test: PropTypes.string,
};

export default ContextMemoTest;
