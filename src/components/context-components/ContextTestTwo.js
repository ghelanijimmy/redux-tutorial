import React from "react";
import useCountRef from "../../hooks/useCountRef";
import { useCustomContext } from "../../context-store/store";

const ContextTestTwo = () => {
  const { getState } = useCustomContext();
  const { test } = getState();
  const num = useCountRef();

  return (
    <div>
      <p>Context: {test}</p>
      <p>Ref Update: {num}</p>
    </div>
  );
};
ContextTestTwo.propTypes = {};

export default ContextTestTwo;
