import React from "react";
import PropTypes from "prop-types";
import useCountRef from "../../hooks/useCountRef";

const ContextTest = ({ test }) => {
  const num = useCountRef();

  return (
    <div>
      <p>Passed Props: {test}</p>
      <p>Ref Update: {num}</p>
    </div>
  );
};
ContextTest.propTypes = {
  test: PropTypes.string,
};

export default ContextTest;
