import React from "react";
import { useCustomContext } from "../../context-store/store";
import { changeAge, changeUser } from "../../context-store/actions";
import useCountRef from "../../hooks/useCountRef";

const ContextChangeNameAge = () => {
  const { dispatch } = useCustomContext();
  const num = useCountRef();

  return (
    <div>
      <button onClick={() => dispatch(changeUser())}>
        Change name (timeout)
      </button>
      <button onClick={() => dispatch(changeAge())}>
        Change age (Promise)
      </button>
      <p>Ref Update: {num}</p>
    </div>
  );
};

export default ContextChangeNameAge;
